﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kolekcionar {
    class Program {
        static void Main(string[] args) {
            var context = new AutomobiliEntities();

            // Unos automobila
            var noviAutomobil = new Automobili() {
                Proizvodjac = "Porsche",
                Model = "Carrera 911",
                Godiste = 2020
            };

            context.Automobili.Add(noviAutomobil);
            context.SaveChanges(); // postoji i context.SaveChangesAsync();

            /*var bugatti =
                (from a in context.Automobili
                 where a.Proizvodjac == "Bugatti"
                 select a).First();

            context.Automobili.Remove(bugatti);
            context.SaveChanges();*/

            var golf = context.Automobili.Find(27);
            golf.Model = "Golf R";
            context.SaveChanges();

            var automobili =
                from a in context.Automobili
                orderby a.Proizvodjac, a.Model
                select a;

            foreach (Automobili a in automobili) {
                Console.WriteLine(a);
            }

            context.Dispose();
        }
    }
}

// AutomobiliEntities.cs
namespace Kolekcionar {
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AutomobiliEntities : DbContext {
        public AutomobiliEntities()
            : base("name=AutomobiliEntities") {
        }

        public virtual DbSet<Automobili> Automobili { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
        }
    }
}

// Automobili.cs
namespace Kolekcionar
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Automobili")]
    public partial class Automobili
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Proizvodjac { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        public int? Godiste { get; set; }

        public override string ToString() {
            return string.Format("{0} {1} ({2})", Proizvodjac, Model, Godiste);
        }
    }
}
